
import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

import PageNotFound from '../Container/Pagenotfound';
// import withHeaderWrapper from '../Component/Comman/Header/withHeaderWrapper';
import Home from '../Container/Home';
import Albums from '../Container/SpotifyAlbums';

const routes = (props) => {
    return (
        <Router>
            <Switch>
                <Route
                    exact
                    path="/"
                    component={Home} />
                <Route
                    exact
                    path="/albums"
                    component={Albums} />
                <Route
                    path=""
                    component={PageNotFound} />
            </Switch>
        </Router>
    );
}

export default routes;