import * as types from './ActionTypes';

const initialState = {
    userToken: {},
    userData: {},
    userAlbumsList: {},
    userNotesList: {},
}

export default (state = initialState, action) => {
    switch (action.type) {
        case types.SET_TOKEN:
            return {
                ...state,
                userToken: action.payload,
            }
        case types.SET_USERDATA:
            return {
                ...state,
                userData: action.payload,
            }
        case types.SET_USER_ALBUMS_LIST:
            return {
                ...state,
                userAlbumsList: action.payload,
            }
        case types.SET_NOTES_LIST:
            return {
                ...state,
                userNotesList: action.payload,
            }
        default:
            return state
    }
}
