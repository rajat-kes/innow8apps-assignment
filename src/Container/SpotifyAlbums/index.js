import React, { Component } from 'react'

import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import * as action from './Actions';

import AlbumList from '../../Component/AlbumCard';
export class Albums extends Component {
    render() {
        return (
            <div style={{padding: '30px'}}>
                <AlbumList />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        albumList: state.albums.userAlbumsList,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        ...action
    }, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(Albums)
