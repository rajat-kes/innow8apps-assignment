const pre = 'SPOTIFY_';

export const SET_TOKEN = pre + 'SET_TOKEN'

export const SET_USERDATA = pre + 'SET_USERDATA'

export const SET_USER_ALBUMS_LIST = pre + 'SET_USER_ALBUMS_LIST'

export const SET_NOTES_LIST = pre + 'SET_NOTES_LIST'