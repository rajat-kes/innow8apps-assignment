import React, { Component } from 'react'

import axios from 'axios';
import qs from 'query-string';
import { Link } from 'react-router-dom';
import './style.css';
export class Home extends Component {
    componentDidMount = () => {
        const refreshToken = new URLSearchParams(this.props.location.search).get("code");
        if (refreshToken) {
            const CLIENT_ID = 'a5648625f1c54e6c8a14257c539995d3';
            const CLIENT_SECRET = 'b9a5988bc558477692633355c41c84fb';
            const url = 'https://accounts.spotify.com/api/token';
            const data = { 'grant_type': 'authorization_code', 'code': refreshToken, 'redirect_uri': encodeURIComponent(window.location.origin.toString()) };
            const options = {
                method: 'POST',
                headers: { 'content-type': 'application/x-www-form-urlencoded', 'Authorization': `Basic ${window.btoa(CLIENT_ID + ":" + CLIENT_SECRET)}` },
                data: qs.stringify(data),
                url,
            };
            axios(options)
                .then(res => {
                    console.log(res);
                })
                .catch(err => {
                    console.log({ err });
                })
        }
    }
    render() {
        return (
            <header className="masthead">
                <div className="container h-100">
                    <div className="h-100 align-items-center justify-content-center text-center">
                        <div className="align-self-end">
                            <h1 className="text-uppercase text-white font-weight-bold">
                                {`Welcome to this project`}
                            </h1>
                            <hr className="divider my-4" />
                        </div>
                        <div className="align-self-baseline">
                            <p className="text-white-75 font-weight-light mb-5">
                                {`This Project use React js designed by Rajat Keserwani`}
                            </p>
                            <a className="btn btn-primary btn-xl js-scroll-trigger" href={`https://accounts.spotify.com/authorize?client_id=a5648625f1c54e6c8a14257c539995d3&response_type=code&redirect_uri=${encodeURIComponent(window.location.origin.toString() + '/')}`}>{`Login with spotify`}</a>
                            <Link className="btn btn-primary btn-xl js-scroll-trigger" to='/albums'>{`Check static value`}</Link>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}

export default Home
