import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const Notes = (props) => {
    const { open, handleClose, title, albumDescription } = props
    return (
        <React.Fragment>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">{title || 'Album Title'}</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {albumDescription || 'Album description (Describe album details)'}
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Notes"
                        type="notes"
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        {'Cancel'}
                    </Button>
                    <Button onClick={handleClose} color="primary">
                        {'Save Changes'}
                    </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}

export default Notes;