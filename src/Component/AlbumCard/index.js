import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import CardMediaImage from '../../Asset/images/albums1.jpg';
import Note from './Notes';
const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
}));

const RecipeReviewCard = (props) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    return (
        <React.Fragment>
            <Note
                handleClose={handleClose}
                open={open} />
            <Card className={classes.root}>
                <CardHeader
                    action={
                        <IconButton aria-label="Edit" onClick={handleClickOpen}>
                            <EditIcon />
                        </IconButton>
                    }
                    title="Album Name"
                    subheader="Album Details"
                />
                <CardMedia
                    className={classes.media}
                    image={CardMediaImage}
                    title="Paella dish"
                />
            </Card>
        </React.Fragment>
    );
}

export default RecipeReviewCard;