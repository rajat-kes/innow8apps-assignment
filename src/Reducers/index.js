import storage from 'redux-persist/lib/storage'
import { persistCombineReducers } from 'redux-persist'

import Albums from '../Container/SpotifyAlbums/reducer';

const reducers = {
    'albums': Albums
}

export const persistConfig = {
    key: 'PROJECT.0.0.1',
    storage,
    blacklist: [
        'albums'
    ]
}

const appReducer = persistCombineReducers(persistConfig, reducers)

const rootReducer = (state, action) => {
    return appReducer(state, action)
};

export default rootReducer;